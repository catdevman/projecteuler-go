package main

import (
	"fmt"
	_ "log"
	"testing"
)

func TestFib(t *testing.T) {
	testSeq := []int{1, 1, 2, 3, 5, 8, 13, 21, 34, 55}
	f := fib()
	for _, v := range testSeq {
		fib := f()
		if fib != v {
			fmt.Println(fib, "not equal to", v)
			t.Fail()
		}
	}
}
