package main

import (
	"fmt"
	"math"
)

type primeTracker struct {
	count int
	last  int
}

func IsPrime(value int) bool {
	for i := 2; i <= int(math.Floor(float64(value)/2)); i++ {
		if value%i == 0 {
			return false
		}
	}
	return value > 1
}

func main() {
	tracker := primeTracker{}
	index := 0
	for {
		if IsPrime(index) {
			tracker.count++
			tracker.last = index
			if tracker.count == 10001 {
				fmt.Println("10001th prime:", tracker.last)
				break
			}
		}
		index++
	}

}
