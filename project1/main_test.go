package main

import (
	_ "log"
	"testing"
)

func TestDivisibleBy(t *testing.T) {
	first := divisibleBy(3, 3)
	if first != true {
		t.Fail()
	}

	second := divisibleBy(5, 5)
	if second != true {
		t.Fail()
	}
}

func TestNotDivisibleBy(t *testing.T) {
	first := divisibleBy(3, 5)
	if first != false {
		t.Fail()
	}

	second := divisibleBy(5, 3)
	if second != false {
		t.Fail()
	}
}
